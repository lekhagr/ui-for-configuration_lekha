/**
 * @author Pushpa Gudmalwar
 * @name webApp.controller: webSourceController
 * @description
 * #webSourceController
 * It is a web article source controller.
 * This controller is responsible for showing the details of the page.
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 * @requires $filter
 * @requires exportToExcelService
 * 
 * @property {String} current : String, This holds current page name.
 * @property {array} rowLimitOptionArray : array This holds entries in dropdown options.
 * @property {String} selected : number, default set from rowLimitOptionArray[1].
 * @property {array} webSourceList: array this holds all details displayed in web article source page.
 */
app.controller("webSourceController", function ($rootScope, $scope, $http, $filter, exportToExcelService) {
	var webSourceList;
	$scope.menu.current = "webSource";

	/**
	 * Asynchronous rest call
	 * @description - http GET request to get all web source. 
	 */
	$http({
		method: "GET",
		url: 'source/web'
	}).then(function (response) {
		$scope.webSourceList = response.data;
		$scope.exportInfo = angular.copy(response.data);
	}, function (reason) {
		$scope.error = reason.data;
	});
	$scope.sortColumn = "site";
	$scope.reverseSort = false;

	/**
	 * Sorting coloumn
	 * @description - Sorting values by coloumn
	 * @param {string} column - coloum name
	 */
	$scope.sortData = function (column) {
		$scope.reverseSort = ($scope.sortColumn == column) ? !$scope.reverseSort : false;
		$scope.sortColumn = column;
	}

	/**
	 * Geting class name
	 * @description - Getting arrow up or arrow down values
	 * @param {string} column - column name 
	 */
	$scope.getSortClass = function (column) {
		if ($scope.sortColumn == column) {
			return $scope.reverseSort ? 'arrow-down' : 'arrow-up';
		}
	}

	// OptionValues
	$scope.rowLimitOptionArray = [10, 20, 30, 50, 100];
	$scope.selected = {
		rowLimit: $scope.rowLimitOptionArray[1]
	};

	/** 
	 * Export to Excel
	 * @description - Export to Excel all web Source.
	 */
	$scope.export = function () {
		var table = $scope.exportInfo;
		table = $filter('orderBy')(table, 'link');
		var csvString = '<table><tr><td>RSS LINK</td><td>WEBSITE<td></tr>';
		for (var i = 0; i < table.length; i++) {
			var rowData = table[i];
			var displayUrl;
			if (rowData.htmlUrl == null) {
				displayUrl = rowData.site;
			} else {
				displayUrl = rowData.htmlUrl;
			}
			csvString = csvString + "<tr><td>" + rowData.link + "</td><td>" + displayUrl + "</td>";
			csvString = csvString + "</tr>";
		}
		csvString += "</table>";
		csvString = csvString.substring(0, csvString.length);
		exportToExcelService.converttoExcel(csvString, 'Web_Source');
	}

	/** 
	 * Export to pdf
	 * @description - Export to Edfxcel webbsourceords.
	 */
	$scope.exportpdf = function () {
		//endDocument();
		var item = $scope.exportInfo;
		var doc = new jsPDF();

		var col = ["RSS LINK", "WEBSITE"];
		var rows = [];
		item = $filter('orderBy')(item, 'link');
		for (var i = 0; i < item.length; i++) {
			var rowData = item[i];
			var displayUrl;
			if (rowData.htmlUrl == null) {
				displayUrl = rowData.site;
			} else {
				displayUrl = rowData.htmlUrl;
			}
			var temp = [rowData.link, displayUrl];
			rows.push(temp);
		}
		doc.autoTable(col, rows);
		doc.save('Web_Source' + new Date().toDateString() + '.pdf');
	};

});