/**
 * @author Pushpa Gudmalwar
 * @name webApp.controller: companyController
 * @description
 * #companyController
 * It is a company controller.
 * This controller is responsible for showing the details of the page.
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 * @requires $q
 * @requires $filter
 * @requires Flash
 * @requires exportToExcelService
 * 
 * @property {String} current : String, This holds current page name.
 * @property {array} rowLimitOptionArray : array This holds entries in dropdown options.
 * @property {String} selected : number, default set from rowLimitOptionArray[1].
 * @property {array} companyLists: array this holds all details displayed in screen.
 */

app.controller("companyController", function ($rootScope, $scope, $http, $q, Flash, $filter, exportToExcelService) {
	var companyLists;
	$scope.menu.current = "company";
	//OptionValues	
	$scope.rowLimitOptionArray = [10, 20, 30, 50, 100];
	$scope.selected = {
		rowLimit: $scope.rowLimitOptionArray[1]
	}
	$scope.companyLists = companyLists;
	var deferred = $q.defer();

	/**
	 * Asynchronous rest call
	 * @description - http GET request to get all company details. 
	 */
	getCompanyList = function () {
		$http({
				method: "GET",
				url: 'company'
			})
			.then(function (response) {
				response.data.forEach(function (_company) {
					_company.isEdit = false;
					_company.companyGram = _company.grams;
				});
				$scope.companyLists = response.data;
				$scope.exportInfo = angular.copy(response.data);
				deferred.resolve(response);
			}, function (reason) {
				$scope.error = reason.data;
				deferred.reject(err);
			});
		return deferred.promise;
	}
	getCompanyList(); //on load

	/**
	 * Editing company grams
	 * @description - Edit company grams.
	 * @param {object} _company - Contains single company details.
	 * @param {string} Oldgram - Previous value of company gram.
	 */
	$scope.editGram = function (_company, Oldgram) {
		var newGram;
		$scope.companyLists.forEach(function (x) {
			if (x.id == _company.id) {
				x.isEdit = true;
				newGram = x.grams;
			} else
				x.isEdit = false;
		})
		Oldgram.replace(/,\s*$/, "");
		_company.companyGram = newGram + ',';
	};

	/**
	 * Saves edited company gram
	 * @description - Assigning edited comapny gram value to current company gram
	 * @param {object} _company - Contains single company detail. 
	 */
	$scope.doneEditing = function (_company) {
		_company.isEdit = false;
		_gram = _company.companyGram.replace(/,\s*$/, "");
		var companydata = {
			"grams": _gram.toLowerCase()
		};
		$http({
			method: "PUT",
			url: 'company/' + _company.id,
			data: companydata
		}).then(function (response) {
			getCompanyList();
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _gram.toUpperCase() + '</strong> edited successfully';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _gram.toUpperCase() + '</strong> is pending for approval  .';
				var id = Flash.create('success', message);
			}
		}, function (reason) {
			$scope.error = reason.data;
			var message = '<strong>' + _gram.toUpperCase() + '</strong> unable to edit';
			var id = Flash.create('danger', message);
		});
	};

	/**
	 * Cancels editing company gram
	 * @description - Cancels company gram editing process.
	 * @param {object} _company - Contains single company information.
	 */
	$scope.cancel = function (_company) {
		_company.isEdit = false;
	};

	/**
	 * Exports to excel
	 * @description - Export to excel all company details.
	 */
	$scope.export = function () {
		var table = $scope.exportInfo;
		table = $filter('orderBy')(table, 'name');
		var csvString = '<table><tr><td>Company Full Name</td><td>Company Abbreviated Name</td><td>Scrip Code</td><td>Scrip Id</td><td>Company Gram(s)</td></tr>';
		for (var i = 0; i < table.length; i++) {
			var rowData = table[i];
			csvString = csvString + "<tr><td>" + rowData.name + "</td><td>" + rowData.abbrName + "</td><td>" +
				rowData.scripCode + "</td><td>" + rowData.scripId + "</td><td>" + rowData.companyGram + "</td>";
			csvString = csvString + "</tr>";
		}
		csvString += "</table>";
		csvString = csvString.substring(0, csvString.length);
		exportToExcelService.converttoExcel(csvString, 'Company');
	}

	/** 
	 * Export to pdf
	 * @description - Export to pdf company details.
	 */
	$scope.exportpdf = function () {
		{
			var item = $scope.exportInfo;
			item = $filter('orderBy')(item, 'name');
			var doc = new jsPDF();
			var col = ["Company Full Name", "Company Abbreviated Name", "Scrip Code", "Scrip Id", "Company Gram(s)"];
			var rows = [];
			var dateInfo, statusInfo;
			for (var i = 0; i < item.length; i++) {
				var rowData = item[i];
				dateInfo = new Date(rowData.startDate);
				var temp = [rowData.name, rowData.abbrName, rowData.scripCode, rowData.scripId, rowData.companyGram];
				rows.push(temp);
			}
			doc.autoTable(col, rows);
			doc.save('Company' + new Date().toDateString() + '.pdf');
		}
	};
});