/**
 * @author Pushpa Gudmalwar
 * @name webApp.controller: changePasswordController
 * @description
 * #changePasswordController
 * It is change password controller.
 * This controller is responsible for change password page.
 * @requires $scope
 * @requires $http
 * @requires Flash
 */

app.controller('changePasswordController', function ($scope,$http,Flash) {
	//to show required conditions for new password field
	$('#pwmatch').hide();
$("input[type=password]").keyup(function(){
    var ucase = new RegExp("[A-Z]+");
	var lcase = new RegExp("[a-z]+");
	var num = new RegExp("[0-9]+");
	var spchar = new RegExp("[!@#$%^&*()_ ]+");
	
	if($("#password1").val().length >= 8){
		$("#8char").removeClass("glyphicon-remove");
		$("#8char").addClass("glyphicon-ok");
		$("#8char").css("color","#00A41E");
	}else{
		$("#8char").removeClass("glyphicon-ok");
		$("#8char").addClass("glyphicon-remove");
		$("#8char").css("color","#FF0004");
	}
	
	if(ucase.test($("#password1").val())){
		$("#ucase").removeClass("glyphicon-remove");
		$("#ucase").addClass("glyphicon-ok");
		$("#ucase").css("color","#00A41E");
	}else{
		$("#ucase").removeClass("glyphicon-ok");
		$("#ucase").addClass("glyphicon-remove");
		$("#ucase").css("color","#FF0004");
	}
	
	if(lcase.test($("#password1").val())){
		$("#lcase").removeClass("glyphicon-remove");
		$("#lcase").addClass("glyphicon-ok");
		$("#lcase").css("color","#00A41E");
	}else{
		$("#lcase").removeClass("glyphicon-ok");
		$("#lcase").addClass("glyphicon-remove");
		$("#lcase").css("color","#FF0004");
	}
	
	if(num.test($("#password1").val())){
		$("#num").removeClass("glyphicon-remove");
		$("#num").addClass("glyphicon-ok");
		$("#num").css("color","#00A41E");
	}else{
		$("#num").removeClass("glyphicon-ok");
		$("#num").addClass("glyphicon-remove");
		$("#num").css("color","#FF0004");
	}
	
	if(spchar.test($("#password1").val())){
		$("#spchar").removeClass("glyphicon-remove");
		$("#spchar").addClass("glyphicon-ok");
		$("#spchar").css("color","#00A41E");
	}else{
		$("#spchar").removeClass("glyphicon-ok");
		$("#spchar").addClass("glyphicon-remove");
		$("#spchar").css("color","#FF0004");
	}		

	if($('#currentPass').val() == $('#password1').val()){
		$('#changePass').attr("disabled","disabled");
		$('#pwmatch').show();
	}else{
		$('#pwmatch').hide();
	}
});

/**
 * Change password 
 * @description - Changing password.
 * @param {object} credentials -User credentials. 
 */
$scope.changePassword = function (credentials) {
	var changeCredentials = {
			"password": credentials.currentPassword,
			"newPassword": credentials.newPassword,
			"confirmNewPassword": credentials.confirmPassword  
		};
	
		$http({
			method: "PUT",
			url: 'users/change-password',
			data: changeCredentials
		}).then(function (response) {
			var message = '<strong>Well done!</strong> You successfully changed your password.You can login now.';
			var id = Flash.create('success', message);
		}, function (reason) {
			var message = '<strong>Error</strong> Unable to change password.';
			var id = Flash.create('danger', message);
			$scope.error = reason.data;
		}); 
        $scope.changeCredentials = null;
}
	
});