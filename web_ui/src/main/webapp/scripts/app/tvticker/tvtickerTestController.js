/**
 * @author Jayashri Nagpure
 * @name webApp.controller: tvtickerTestController
 * @description
 * #tvtickerTestController
 * It is a video to text controller.
 * This controller is responsible for showing the details video to text conversion.
 * @requires $state
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 */
	app.controller("tvtickerTestController",function($rootScope,$scope, $http, $q, Flash,$filter,exportToExcelService){
	var companyLists;
	$scope.menu.current="tvticker";
	$scope.rowLimitOptionArray = [10,20,30,50,100];  
	$scope.selected = {rowLimit:$scope.rowLimitOptionArray[1]}
	$scope.companyLists = companyLists;
	var deferred = $q.defer();
	$scope.date = new Date();

	
	getCompanyList = function(){
		$http({
			method : "GET",
			url	: 'company'
		})
		.then(function(response){
			$scope.companyLists = response.data;
			$scope.exportInfo = angular.copy(response.data);
			deferred.resolve(response);
		},function(reason){
			$scope.error = reason.data;
			deferred.reject(err);
		});
		return deferred.promise;
	}
	getCompanyList();
	  
});