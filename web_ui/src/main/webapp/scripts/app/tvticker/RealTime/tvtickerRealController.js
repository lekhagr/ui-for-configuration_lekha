/**
 * @author Lekha
 * @name webApp.controller: tvtickerRealController
 * @description
 * #tvtickerRealController
 * It is a video to text controller.
 * This controller is responsible for showing the Real time details of TV tickers listing top 10 tickers
 * @requires $state
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 */
	app.controller("tvtickerRealController",function($rootScope,$scope, $http, $q, Flash,$filter,exportToExcelService){
	var companyLists;
	$scope.menu.current="tvticker";
	$scope.rowLimitOptionArray = [10,20,30,50,100];  
	$scope.selected = {rowLimit:$scope.rowLimitOptionArray[1]}
	$scope.companyLists = companyLists;
	var deferred = $q.defer();


	
	getCompanyList = function(){
		/*var count =0;
		$interval(function() {
	        $scope.displayMsg = "Refreshed " + count + " counter time.";
	        count++;
		}, 1000,5);  
		*/
		$http({
			method : "GET",
			url	: 'company'
		})
		.then(function(response){
			$scope.companyLists = response.data;
			$scope.exportInfo = angular.copy(response.data);
			deferred.resolve(response);
		},function(reason){
			$scope.error = reason.data;
			deferred.reject(err);
		});
		return deferred.promise;
	
	}
	getCompanyList();
	
	
	  
});