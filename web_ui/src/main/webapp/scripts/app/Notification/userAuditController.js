/**
 * @author Pushpa Gudmalwar
 * @name webApp.controller: userAuditController
 * @description
 * #userAuditController
 * It is a user audit controller.
 * This controller is responsible for showing the details of transactions done.
 * @requires $state
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 * @requires $filter
 * @requires Flash
 * @requires exportToExcelService
 * @requires formatdateService
 * 
 * @property {array} rowLimitOptionArray : array This holds entries in dropdown options.
 * @property {String} selected : number, default set from rowLimitOptionArray[1].
 * @property {array} allRequestList: array this holds all details displayed in screen.
 */

app.controller('userAuditController',function($rootScope,$state,Flash,$scope,$http,$filter,exportToExcelService,formatdateService) {
		$scope.rowLimitOptionArray = [10,20,30,50,100];  
		$scope.selected = {rowLimit:$scope.rowLimitOptionArray[1]};
		// on load Date should be blank to get all data
		$scope.startDate = '';
		$scope.endDate = '';

		/**
		 * Get list of all requests		
		 * @description - Gives all requests.		
		 * @callback requestCallback
		 * @param {requestCallback} response - The callback that handles the response.
		 * @param {list} response.data - List of all requests.
		 * @var {list} allRequestList - List of all requests.
		 * @var {list} exportInfo - List contains response data.
		 */
	 	var successCallBack = function(response){
	     	$scope.allRequestList = response.data;
	 		$scope.allRequestList = formatdateService.formatdate(response.data);
	     	$scope.allRequestList.forEach(function(element){
	     		if(element.requestType.requestMedia == 'all'){
		     		element.companyrequestedValue = element.requestedValue.substring(element.requestedValue.indexOf(':')+1,element.requestedValue.length);	
	     		}
	     		if(element.requestedOperation == 'ADD'){
		     		element.addexistingValue = '';	
	     		}
	     	})
	     	
	 		$scope.exportInfo = angular.copy(response.data);
	      };

		/**
		 * Get Error information.	
		 * @description - Gives error information.		
		 * @callback requestCallback
		 * @param {requestCallback} reason - The callback that handles the response.
	     * @param {object} reason.data - List of pending requests.
		 * @var {object} error - Gives error details.
		 */
		var errorCallBack = function(reason){
			$scope.error = reason.data;
		};

		/**
		 * Asynchronous rest call
		 * @description - http GET request to get all requets. 
		 * @param {string} status - status of request (all,approved,rejected).
		 */		
		$scope.getAllRequestList = function(status){
			var _url;
			 if (status == 'all')
				_url = 'request';
			$http({
					method: "GET",
					url: _url
				})
				.then(successCallBack, errorCallBack);
		 };
			 
		 $scope.getAllRequestList('all');        // on load 
	
		/**
		 * Exports to excel
		 * @description - Export to excel all requests.
		 */
		$scope.export = function(){
			var table = $scope.exportInfo;
			table = $filter('orderBy')(table, 'id');
				var csvString = '<table><tr><td>Request Id</td>'+
					'<td>Requested By</td>'+
					'<td>Requested Date</td>'+
					'<td>Approved By</td>'+
					'<td>Approved Date</td>'+
					'<td>Existing Value</td>'+
					'<td>Requested Value</td>'+
					'<td>Requested Operation</td>'+
					'<td>Requested Type</td>'+
					'<td>Requested Media</td>'+
					'<td>Status</td></tr>';
				var requestedDate, actionedDate;
				for(var i=0; i<table.length;i++){
					var rowData = table[i];
					if(rowData.requestedOperation == 'ADD'){
						rowData.existingValue = rowData.addexistingValue;
					}
					if(rowData.requestType.requestMedia == 'all'){
						rowData.requestedValue = rowData.companyrequestedValue
					}
					if(rowData.actionedBy == null ){
						rowData.actionedBy = '';
					}
					if(rowData.actionedDate == null ){
						rowData.actionedDate = '';
					}
					
					csvString = csvString + "<tr><td>" +rowData.id + "</td><td>" + rowData.requestedBy+"</td><td>" + rowData.requestedDate+"</td><td>" 
					+ rowData.actionedBy+"</td><td>" + rowData.actionedDate+"</td><td>" + rowData.existingValue+"</td><td>" + rowData.requestedValue+"</td><td>"
					+ rowData.requestedOperation+"</td><td>" + rowData.requestType.requestType+"</td><td>" + rowData.requestType.requestMedia+"</td><td>"+ rowData.requestStatus+"</td>";
					csvString = csvString + "</tr>";
				} 
				
			csvString += "</table>";
			csvString = csvString.substring(0, csvString.length);
			exportToExcelService.converttoExcel(csvString,'User_Audit');	        
		}

		/** 
		 * Export to pdf
		 * @description - Export to pdf all requests.
		*/
		$scope.exportpdf = function () {
			{
				var item = $scope.exportInfo;
				item = $filter('orderBy')(item, 'id');
				var doc = new jsPDF('landscape'); 
				var col = ["Request Id","Requested By","Requested Date","Approved By","Approved Date","Existing Value","Requested Value","Requested Operation","Requested Type","Requested Media","Status"];
				var rows = [];
				var requestedDate, actionedDate;    	    
				for(var i=0; i<item.length;i++){
					var rowData = item[i];
					if(rowData.requestedOperation == 'ADD'){
						rowData.existingValue = rowData.addexistingValue;
					}
					if(rowData.requestType.requestMedia == 'all'){
						rowData.requestedValue = rowData.companyrequestedValue
					}
					var d = new Date();
				if(rowData.actionedBy == null ){
					rowData.actionedBy = '';
				}
				if(rowData.actionedDate == null ){
					rowData.actionedDate = '';
				}

					var	temp = [rowData.id,rowData.requestedBy,rowData.requestedDate,rowData.actionedBy,rowData.actionedDate,rowData.existingValue, rowData.requestedValue,
						rowData.requestedOperation,rowData.requestType.requestType,rowData.requestType.requestMedia,rowData.requestStatus];
					rows.push(temp);
				}
			doc.autoTable(col, rows);
			doc.save('User_Audit '+ new Date().toDateString() +'.pdf');
			}
		};
	
	});




