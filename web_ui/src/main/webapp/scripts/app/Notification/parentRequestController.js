/**
 * @author Jayashri Nagpure
 * @name webApp.controller: parentRequestController
 * @description
 * #parentRequestController
 * It is parent request controller.
 * This controller is responsible for providing container to load all notifications.
 * @requires $rootScope
 * @requires $state
 * @requires $scope
 * 
 * @property {array} menu : array This holds heading for current page.
 */
app.controller('parentRequestController',function($rootScope,$state,$scope) {
	$scope.menu = {current:'all'};
		$state.go('parentRequest.all');
	});