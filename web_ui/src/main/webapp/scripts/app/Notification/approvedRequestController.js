/**
 * @author Jayashri Nagpure
 * @name webApp.controller: approvedRequestController
 * @description
 * #approvedRequestController
 * It is approved request controller.
 * This controller is responsible for showing the details of the all requests in page.
 * @requires $state
 * @requires $scope
 * @requires $http
 * @requires $filter
 * @requires Flash
 * @requires exportToExcelService
 * 
 * @property {String} current : String, This holds current page name.
 * @property {array} rowLimitOptionArray : array This holds entries in dropdown options.
 * @property {String} selected : number, default set from rowLimitOptionArray[1].
 * @property {array} allRequestList: array this holds all details displayed in screen.
 */
app.controller('approvedRequestController',function($state,Flash,$scope,$http,$filter,exportToExcelService) {
		$scope.menu.current="approved";
		$scope.message = "Pending Request Page";
		//var PendingRequestList;
		$scope.rowLimitOptionArray = [10,15,30,50,100];  
		$scope.selected = {rowLimit:$scope.rowLimitOptionArray[1]};

		/**
		 * Get list of all requests		
		 * @description - Gives approved requests.		
		 * @callback requestCallback
		 * @param {requestCallback} response - The callback that handles the response.
		 * @param {list} response.data - List of approved requests.
		 * @var {list} allRequestList - List of approved requests.
		 * @var {list} exportInfo - List contains response data.
		 */
		var successCallBack = function(response){
	     	$scope.allRequestList = response.data;
	     	$scope.allRequestList.forEach(function(element){
	     		if(element.requestType.requestMedia == 'all'){
		     		element.companyrequestedValue = element.requestedValue.substring(element.requestedValue.indexOf(':')+1,element.requestedValue.length);	
	     		}
	     		if(element.requestedOperation == 'ADD'){
		     		element.addexistingValue = '';	
	     		}
	     	})

	 		$scope.exportInfo = angular.copy(response.data);
	      };

		/**
		 * Get Error information.	
		 * @description - Gives error information.		
		 * @callback requestCallback
		 * @param {requestCallback} reason - The callback that handles the response.
	     * @param {object} reason.data - List of pending requests.
		 * @var {object} error - Gives error details.
		 */ 
		var errorCallBack = function(reason){
			$scope.error = reason.data;
		};
		
		/**
		 * Asynchronous rest call
		 * @description - http GET request to get approved requets.
		 * @param {string} status - status of request (all,approved,rejected).
		 */
		getAllRequestList = function(status){
			var _url;
			 if (status == 'accepted')
				_url = 'request/accepted';
			$http({
					method: "GET",
					url: _url
				})
				.then(successCallBack, errorCallBack);
		 };
		
		 getAllRequestList('accepted'); //on load
		 
		/**
		 * Exports to excel
		 * @description - Export to excel all requests.
		 */
		$scope.export = function(){
			var table = $scope.exportInfo;
			table = $filter('orderBy')(table, 'id');
				var csvString = '<table><tr><td>Request Id</td><td>Requested By</td><td>Requested Operation</td><td>Existing Value</td><td>Requested Value</td><td>Media Type</td><td>Category</td><td>Approvers Comment</td><td>Approved By</td></tr>';
				for(var i=0; i<table.length;i++){
					var rowData = table[i];
					if(rowData.requestedOperation == 'ADD'){
						rowData.existingValue = rowData.addexistingValue;
					}
					if(rowData.requestType.requestMedia == 'all'){
						rowData.requestedValue = rowData.companyrequestedValue
					}

					csvString = csvString + "<tr><td>" +rowData.id + "</td><td>" + rowData.requestedBy+"</td><td>" 
					+ rowData.requestedOperation+ "</td><td>"+ rowData.existingValue + "</td><td>"+ rowData.requestedValue + "</td><td>" +rowData.requestType.requestType + "</td><td>"+rowData.requestType.requestMedia+"</td><td>" + rowData.comment+"</td><td>"+rowData.actionedBy+"</td>";
					csvString = csvString + "</tr>";
				}
			csvString += "</table>";
			csvString = csvString.substring(0, csvString.length);
			exportToExcelService.converttoExcel(csvString,'Approved_Requests');	        
		}

		/** 
		 * Export to pdf
		 * @description - Export to pdf all requests.
		*/
		$scope.exportpdf = function () {
			{
				var item = $scope.exportInfo;
				item = $filter('orderBy')(item, 'id');
				var doc = new jsPDF('l','cm',[30,30]); 
				var col = ["Request Id","Requested By","Requested Operation","Existing Value","Requested Value","Media Type","Category","Approvers Comment","Approved By"];
				var rows = [];
				var dateInfo,statusInfo;	    	    
				for(var i=0; i<item.length;i++){
					var rowData = item[i];
					if(rowData.requestedOperation == 'ADD'){
						rowData.existingValue = rowData.addexistingValue;
					}
					if(rowData.requestType.requestMedia == 'all'){
						rowData.requestedValue = rowData.companyrequestedValue
					}

					var	temp = [rowData.id, rowData.requestedBy, rowData.requestedOperation, rowData.existingValue, rowData.requestedValue, rowData.requestType.requestType, rowData.requestType.requestMedia,rowData.comment,rowData.actionedBy];
					rows.push(temp);
				}
			doc.autoTable(col, rows);
			doc.save('Approved_Requests'+ new Date().toDateString() +'.pdf');
			}
		};
	
	});