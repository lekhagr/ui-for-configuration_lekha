/**
 * @author Jayashri Nagpure
 * @name webApp.filter: embededUrlFilter
 * @description
 * #embededUrlFilter
 * It is youtube url filter.
 * This filter is responsible for filtering youtube url into embeded url.
 */ 
app.filter('embededUrlFilter', ['$sce', function ($sce) {
        return function(url) {
        		if(url)
        		var video_id = url.split('v=')[1].split('&')[0];
            return $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + video_id);
        };
    }]);