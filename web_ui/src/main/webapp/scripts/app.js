/**
 * @module webApp
 * @author Pushpa Gudmalwar
 * @name module: webApp
 * @description
 * #webApp
 * It is a web app module.
 * This is responsible for initiating angularjs application.
 * directive:
 * some are below dependency injections required:
 * @requires ngIdle
 * @requires http-auth-interceptor
 * @requires angular-toasty
 * @requires LocalStorageModule
 * @requires ngFlash
 * @requires dirPagination
 */
	var app = angular.module('webApp', ['ui.router','ngIdle','http-auth-interceptor','angular-toasty','LocalStorageModule','ngFlash','dirPagination','ngSanitize','com.2fdevs.videogular','ngFileUpload'])
	.config(function($stateProvider, $urlRouterProvider,$qProvider,$locationProvider) {
		$locationProvider.hashPrefix('');
		 $qProvider.errorOnUnhandledRejections(false);
		$stateProvider
		.state('signup', {
			url: '/signup',
			templateUrl: 'scripts/app/signup/signup.html',
			controller: 'signupController'
		})
		.state('login', {
			url: '/login',
			templateUrl: 'scripts/app/login/login.html',
			controller: 'loginController'
		})
		.state('reset-password', {
			url: '/reset-password',
			templateUrl: 'scripts/app/login/forgotPassword.html',
			controller: 'resetPasswordController'
		})
		.state('changePassword', {
			url: '/changePassword',
			templateUrl: 'scripts/app/login/changePassword.html',
			controller: 'changePasswordController'
		})
		.state('parentRequest', {
			url: '/request',
			templateUrl: 'scripts/app/Notification/parentRequest.html',
			controller: 'parentRequestController'	
		})
		.state("parentRequest.all",{
			url: '/all',
			templateUrl: 'scripts/app/Notification/allRequest.html',
			controller:'allRequestController'
		})
		.state("parentRequest.pending",{
			url: '/pending',
			templateUrl: 'scripts/app/Notification/pendingRequest.html',
			controller:'pendingRequestController'
		})	
		.state("parentRequest.approved",{
			url: '/approved',
			templateUrl: 'scripts/app/Notification/approvedRequest.html',
			controller:'approvedRequestController'
		})	
		.state("parentRequest.rejected",{
			url: '/rejected',
			templateUrl: 'scripts/app/Notification/rejectedRequest.html',
			controller:'rejectedRequestController'
		})
		
		.state("userAudit",{
			url: '/userAudit',
			templateUrl: 'scripts/app/Notification/userAudit.html',
			controller:'userAuditController'
		})
		.state('logout', {
			url: '/logout',
			controller: 'logoutController'
		})
		.state('homeParent', {
			url: '/',
			templateUrl: 'scripts/app/home/home2.html',
			controller: 'homeController',
			resolve: {

			},
			abstract : true
		})
		.state("homeParent.home",{
			url: 'home',
			templateUrl: 'scripts/app/home/homePage.html',
			controller:'homeChildController'
		})

		//company
		.state("homeParent.company",{
			url: 'company',
			templateUrl: 'scripts/app/company/company.html',
			controller: 'companyController'
		})

        //sources
        .state("homeParent.webSource",{
        	url: 'webSource',
        	templateUrl: 'scripts/app/sources/templates/webSource.html',
        	controller: 'webSourceController'
        })
        .state("homeParent.twitterSource",{
        	url:"twitterSource/:status",
        	params: {status: 'all'},
        	templateUrl: 'scripts/app/sources/templates/twitterSource.html',
        	controller: 'twitterSourceController'
        })
        .state("homeParent.facebookSource",{
        	url:"facebookSource/:status",
        	params: {status: 'all'},
        	templateUrl: 'scripts/app/sources/templates/facebookSource.html',
        	controller: 'facebookSourceController'
        })

	//keywords
	.state("homeParent.webKeyword",{
		url: 'webKeyword/:status',
		params: {status: 'all'},
		templateUrl: 'scripts/app/keywords/templates/webKeyword.html',
		controller: 'webKeywordController'
	})
	.state("homeParent.twitterKeyword",{
		url: "twitterKeyword/:status",
		params: {status: 'all'},
		templateUrl: 'scripts/app/keywords/templates/twitterKeyword.html',
		controller: 'twitterKeywordController'
	})
	.state("homeParent.facebookKeyword",{
		url : 'facebookKeyword/:status',
		params: {status: 'all'},
		templateUrl: 'scripts/app/keywords/templates/facebookKeyword.html',
		controller: 'facebookKeywordController'
	})
	
	//video to text
    .state('homeParent.videoToText', {
        url: 'videototext',
        templateUrl: 'scripts/app/videototext/videototext.html',
        controller: 'videototextController'
    }) 
    .state('homeParent.rumoursVideo', {
        url: 'rumoursVideo',
        templateUrl: 'scripts/app/videototext/rumours_video/rumoursVideo.html',
        controller: 'rumoursVideoController'
    })         

    //TV Ticker
    /*.state('homeParent.tvTicker', {
        url: 'tvticker',
        templateUrl: 'scripts/app/tvticker/tvticker.html',
        controller: 'tvtickerController'
    })*/
      .state('homeParent.tvTicker', {
        url: 'tvticker',
        templateUrl: 'scripts/app/tvticker/tvtickerTest.html',
        controller: 'tvtickerTestController'
    })
    .state('homeParent.tvTickerView', {
        url: 'tvtickerview',
        templateUrl: 'scripts/app/tvticker/View/tvtickerView.html',
        controller: 'tvtickerTestController'
    })
    .state('homeParent.tvTickerReal', {
        url: 'tvtickerreal',
        templateUrl: 'scripts/app/tvticker/RealTime/tvtickerReal.html',
        controller: 'tvtickerRealController'
    })
  
    
	$urlRouterProvider.otherwise('home');
});

app.config(function(IdleProvider, KeepaliveProvider) {
  IdleProvider.idle(10*60); // 10 minutes idle  10*60
  IdleProvider.timeout(30); // after 30 seconds idle, time the user out
})

/**
 * CompilerProvider to download text file. 
*/
app.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(|blob|):/);
}]);

app.run(function($state,$rootScope,localStorageService,$location,$window, $http,Idle){
	'use strict';
    $rootScope.globals = localStorageService.get('globals');
    if($rootScope.globals){
    var authToken = $rootScope.globals.currentUser.authdata;
    $http.defaults.headers.common.Authorization = authToken;
    }
    $rootScope.$on('IdleTimeout', function() {
    	$state.go('logout');
    	
    	Idle.watch();
    });
    Idle.watch();
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
    	if(localStorageService.get('authorizationData')){
    		var user_role = localStorageService.get('authorizationData').user_role.replace(/[^a-zA-Z0-9, ]/g, "").toLowerCase(); 
    		$rootScope.userName = localStorageService.get('authorizationData').userName;
         	// getting user role here and setting global variable 	
    		user_role = user_role.split(/\s*,\s*/);
        	if(user_role[0] == 'super user'){
        		$rootScope.userRole ='super_user' ;
        		$rootScope.isSuperUser = true;
        		$rootScope.permissions = true;
        		$rootScope.isReportViewer = true;
        	}else if(user_role[0] == 'user' &&  user_role[1]=='report viewer'){
        		$rootScope.userRole ='report_user' ;
        		$rootScope.isSuperUser = false;
        		$rootScope.permissions = true;
        		$rootScope.isReportViewer = true;
        	}else if(user_role[0] == 'user' ){
        		$rootScope.userRole ='user' ;
        		$rootScope.isSuperUser = false;
        		$rootScope.permissions = true;
        		$rootScope.isReportViewer = false;        		
        	}else if(user_role[0] == 'viewer' && user_role[1]=='report viewer'){
        		$rootScope.userRole ='report_viewer' ;
        		$rootScope.isSuperUser = false;
        		$rootScope.permissions = false;
        		$rootScope.isReportViewer = true;
            }else if(user_role[0] == 'viewer'){
                $rootScope.userRole ='viewer' ;
                $rootScope.isSuperUser = false;
                $rootScope.permissions = false;
                $rootScope.isReportViewer = false;
            }
    	}
	
        if($rootScope.globals){
    	var userName= $rootScope.globals.currentUser.username; 
       // $rootScope.permissions = true;	
    	    		
    		        var loggedIn= $rootScope.globals.currentUser.authdata;
    		        if (!loggedIn) {
    		        	$state.go('login');
    		        } 
        }
    		 
    });
    
    $rootScope.$on('event:auth-loginConfirmed', function(data) {
        $rootScope.authenticated = true;
    $location.path('/home').replace();
        if ($location.path() === "/login") {
    	$location.path('/home').replace();
    }
    }); 
    $rootScope.$on('event:auth-loginCancelled', function() {
        $rootScope.authenticated = true;
    $state.go('logout');
    }); 
    
    $rootScope.$on('event:auth-loginRequired', function(data) {
		$http.defaults.headers.common.Authorization=null;
		$state.go("login");
        }); 
    $rootScope.$on('event:auth-forbidden', function(data) {
        $state.go('logout');
        
        }); 
});