package com.bse.configurator.exception;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Bharat.Pattani
 *Class ValidationUtil.
 */
public class ValidationUtil {

	/**
	 * To find list of errors occurred from binding.
	 * @param errors
	 * @return list of all errors occurred.
	 */
    public static List<String> fromBindingErrors(Errors errors) {
        List<String> validErrors = new ArrayList<String>();
        for (ObjectError objectError : errors.getAllErrors()) {
            validErrors.add(objectError.getDefaultMessage());
        }
        return validErrors;
    }
}
