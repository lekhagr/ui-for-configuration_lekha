package com.bse.configurator.requests;

import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bse.configurator.company.Company;
import com.bse.configurator.company.CompanyController;
import com.bse.configurator.company.CompanyRepository;
import com.bse.configurator.email.Email;
import com.bse.configurator.email.EmailService;
import com.bse.configurator.exception.ResourceNotFoundException;
import com.bse.configurator.keyword.Keyword;
import com.bse.configurator.keyword.KeywordController;
import com.bse.configurator.keyword.KeywordMedia;
import com.bse.configurator.keyword.KeywordRepository;
import com.bse.configurator.keyword.KeywordType;
import com.bse.configurator.request.type.RequestType;
import com.bse.configurator.request.type.RequestTypeRepository;
import com.bse.configurator.source.SourceRepository;
import com.bse.configurator.source.SourceType;
import com.bse.configurator.source.facebook.FacebookSource;
import com.bse.configurator.source.facebook.FacebookSourceController;
import com.bse.configurator.source.facebook.FacebookSourceRepository;
import com.bse.configurator.source.twitter.TwitterSource;
import com.bse.configurator.source.twitter.TwitterSourceController;
import com.bse.configurator.source.twitter.TwitterSourceRepository;
import com.bse.configurator.user.ApplicationUserRepository;
import com.bse.configurator.util.Constants;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author Pushpa.Gudmalwar
 * Class of RequestController 
 * To perform all operations required to proceed a request.
 */
@RestController
@RequestMapping("/request")
public class RequestController {
	private RequestRepository requestRepository;
	private TwitterSourceRepository twitterSourceRepository;
	private FacebookSourceRepository facebookSourceRepository;
	private KeywordRepository keywordRepository;
	private SourceRepository sourceRepository;
	private CompanyRepository companyRepository;
	private RequestType hashtagSource, handleSource, webSource, userSource, pageSource, groupSource;
	private RequestType company;
	private RequestType selectWeb, rejectWeb, selectFacebook, rejectFacebook, selectTwitter, rejectTwitter;

	private Email email;
	@Autowired
	EmailService emailService;

	@Autowired
	private TwitterSourceController twitterCntrlr;

	@Autowired
	private FacebookSourceController facebookCntrlr;

	@Autowired
	private KeywordController keywordCntrlr;

	@Autowired
	private CompanyController companyCntrlr;

	@Autowired
	public RequestController(RequestRepository requestRepository, RequestTypeRepository requestTypeRepository,
			FacebookSourceRepository facebookSourceRepository, TwitterSourceRepository twitterSourceRepository,
			KeywordRepository keywordRepository, SourceRepository sourceRepository,
			CompanyRepository companyRepository,ApplicationUserRepository applicationUserRepository) {
		this.requestRepository = requestRepository;
		this.sourceRepository = sourceRepository;
		this.facebookSourceRepository = facebookSourceRepository;
		this.twitterSourceRepository = twitterSourceRepository;
		this.keywordRepository = keywordRepository;
		this.companyRepository = companyRepository;
		this.hashtagSource = requestTypeRepository.findBySourceType("hashtag", "twitter");
		this.handleSource = requestTypeRepository.findBySourceType("handle", "twitter");
		this.webSource = requestTypeRepository.findBySourceType("rss", "web");
		this.userSource = requestTypeRepository.findBySourceType("user", "facebook");
		this.pageSource = requestTypeRepository.findBySourceType("page", "facebook");
		this.groupSource = requestTypeRepository.findBySourceType("group", "facebook");
		this.company = requestTypeRepository.findBySourceType("", "company");
		this.selectWeb = requestTypeRepository.findBySourceType("selection", "web");
		this.rejectWeb = requestTypeRepository.findBySourceType("rejection", "web");
		this.selectFacebook = requestTypeRepository.findBySourceType("selection", "facebook");
		this.rejectFacebook = requestTypeRepository.findBySourceType("rejection", "facebook");
		this.selectTwitter = requestTypeRepository.findBySourceType("selection", "twitter");
		this.rejectTwitter = requestTypeRepository.findBySourceType("rejection", "twitter");
		email = new Email();
		email.setTo(applicationUserRepository.findAllEmails());
	}

	private RequestType getRequestType(int id) {
		RequestType requestType = null;
		switch (id) {
		case 1:
			requestType = this.company;
			break;
		case 2:
			requestType = this.webSource;
			break;
		case 3:
			requestType = this.hashtagSource;
			break;
		case 4:
			requestType = this.handleSource;
			break;
		case 5:
			requestType = this.pageSource;
			break;
		case 6:
			requestType = this.groupSource;
			break;
		case 7:
			requestType = this.userSource;
			break;
		case 8:
			requestType = this.selectWeb;
			break;
		case 9:
			requestType = this.rejectWeb;
			break;
		case 10:
			requestType = this.selectTwitter;
			break;
		case 11:
			requestType = this.rejectTwitter;
			break;
		case 12:
			requestType = this.selectFacebook;
			break;
		case 13:
			requestType = this.rejectFacebook;
			break;
		}
		return requestType;
	}

	/**
	 * 
	 * To add/save new request generated by user.
	 * @param request object of current request.
	 * @param requestStatus as in approved,pending or rejected.
	 * @param username name of user.
	 * @param comment added by approver.
	 * @param actionedDate date generated by system when new request gets added.
	 */
	private void addRequest(Request request, RequestStatus requestStatus, String username, String comment,Date actionedDate) {
		request.setRequestStatus(requestStatus);
		request.setActionedDate(actionedDate);
		request.setComment(comment);
		request.setActionedBy(username);
		this.requestRepository.save(request);
	}

	/**
	 * To get all requests generated.
	 * @return list of all requests till now.
	 */
	@GetMapping
	public List<Request> getRequest() {
		return this.requestRepository.findAllRequest();
	}
	/**
	 * To get requests based on request status.
	 * @param requestStatus values are pending, accepted,rejected.
	 * @return list of records of requests based on status.
	 */
	@GetMapping("/{requestStatus:pending|accepted|rejected}")
	public List<Request> getSourcesByStatus(@PathVariable("requestStatus") String requestStatus) {
		switch (requestStatus) {
		case "pending":
			return this.requestRepository.findRecordsByStatus(RequestStatus.PENDING);
		case "accepted":
			return this.requestRepository.findRecordsByStatus(RequestStatus.APPROVED);
		case "rejected":
			return this.requestRepository.findRecordsByStatus(RequestStatus.REJECTED);
		}
		return null;
	}

	/**
	 * To approve pending request based on id.
	 * @param request object of request.
	 * @param id long value of request record.
	 */
	@PutMapping("/pending/{id}")
	public void approvePending(@RequestBody Request request, @PathVariable("id") Long id) {
		boolean authorized = false;
		Date actionedDate = new Date();
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		RequestType currentRequestType = this.getRequestType(request.getRequestType().getId());
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Request requestInfoById = this.requestRepository.findById(id);
		
		if (authorized) {
			if (requestInfoById == null) {
				throw new ResourceNotFoundException((long) 400, "Request information not found.");
			} else {
				// find add/activate/deactivate
				if (requestInfoById.getRequestType().getRequestType().equals("rss")) {// company
				} else if (requestInfoById.getRequestType().getRequestMedia().equals(Constants.COMPANY)) { // web source
					// web for now nothing as data comes from feedly
				} else if (requestInfoById.getRequestType().getRequestType().equals("selection") || requestInfoById.getRequestType().getRequestType().equals("rejection")) {
					KeywordType keywordType = KeywordType.valueOf(currentRequestType.getRequestType().toUpperCase());
					KeywordMedia keywordMedia = KeywordMedia
							.valueOf(currentRequestType.getRequestMedia().toUpperCase());
					Keyword keyword = new Keyword(requestInfoById.getRequestedValue(), keywordType, keywordMedia, true);
					keyword.setRequestedBy(requestInfoById.getRequestedBy());
					keyword.setRequestId(id);
					keyword.setComment(request.getComment());
					keyword.setRequestedDate(requestInfoById.getRequestedDate());
					keyword.setApproveDate(actionedDate);
					keywordCntrlr.addKeyword(keyword, currentRequestType.getRequestMedia());
					this.email.setMessage(String.format("%s approved addition of '%s' %s %s keyword.",username,keyword.getKeyword(),keyword.getMedia(),keyword.getType()));
				}
				else if (requestInfoById.getRequestType().getRequestMedia().equals("twitter")){
					SourceType sourceType = twitterCntrlr.getSourceType(requestInfoById.getRequestType().getRequestType());
					String[] nameOfsource = requestInfoById.getRequestedValue().split("\\|");
					String receivedTwitterSourceName = nameOfsource[0];
					String twitterSourceName = receivedTwitterSourceName.trim();
					String receivedTwitterDisplayName = nameOfsource[1];
					TwitterSource twitterSource = new TwitterSource(twitterSourceName,receivedTwitterDisplayName,
							currentRequestType.getRequestType(), true);
					twitterSource.setRequestedBy(requestInfoById.getRequestedBy());
					twitterSource.setRequestId(id);
					twitterSource.setApproveDate(actionedDate);
					twitterSource.setComment(request.getComment());
					twitterSource.setRequestedDate(requestInfoById.getRequestedDate());
					twitterSource.setSourceType(sourceType);
					twitterCntrlr.addSource(twitterSource);
					this.email.setMessage(String.format("%s approved addition of '%s' twitter %s.",username,twitterSource.getSourceName(),twitterSource.getSourceTypeName()));
				} else if (requestInfoById.getRequestType().getRequestMedia().equals("facebook")) {// fb source
					SourceType sourceType = facebookCntrlr.getSourceType(requestInfoById.getRequestType().getRequestType());
					FacebookSource facebookSource = new FacebookSource(requestInfoById.getRequestedValue(),
							currentRequestType.getRequestType(), true);
					facebookSource.setRequestedBy(requestInfoById.getRequestedBy());
					facebookSource.setRequestId(id);
					facebookSource.setRequestedDate(requestInfoById.getRequestedDate());
					facebookSource.setApproveDate(actionedDate);
					facebookSource.setComment(request.getComment());
					facebookSource.setSourceType(sourceType);
					facebookCntrlr.addSource(facebookSource);
					this.email.setMessage(String.format("%s approved addition of '%s' facebook %s",username,facebookSource.getSourceName(),facebookSource.getSourceTypeName()));
				}
				this.addRequest(requestInfoById, RequestStatus.APPROVED, username, request.getComment(),actionedDate);
				//this.email.setMessage(String.format("deactivated add"));
				this.emailService.send(this.email);
			}
		}
	}

	/**
	 * To reject pending request based on id.
	 * @param request object of request.
	 * @param id long value of request record.
	 */
	@PutMapping("/pending/rejected/{id}")
	public void rejectPending(@RequestBody Request request, @PathVariable("id") Long id) {
		boolean authorized = false;
		Date actionedDate = new Date();
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Request requestInfoById = this.requestRepository.findById(id);
		if (authorized) {
			if (requestInfoById == null) {
				throw new ResourceNotFoundException((long) 400, "Request information not found.");
			} else {
				/*
				 * String oldDetails = requestInfoById.getExistingValue(); String requestInfo =
				 * requestInfoById.getRequestedValue(); String requestDetails = "ADDED: " +
				 * currentRequestType.getRequestType() + " |" +
				 * currentRequestType.getRequestMedia(); Request updatedRequest = new
				 * Request("rejected", currentRequestType, requestDetails, oldDetails,
				 * requestInfoById.getRequestedBy());
				 */
				this.addRequest(requestInfoById, RequestStatus.REJECTED, username, request.getComment(),actionedDate);
				this.email.setMessage(String.format("%s rejected %s operation on '%s' of %s %s.", username,requestInfoById.getRequestedOperation(), requestInfoById.getRequestedValue(),requestInfoById.getRequestType().getRequestMedia() ,requestInfoById.getRequestType().getRequestType()));
				this.emailService.send(this.email);
			}
		}
	}

	/**
	 * To accept pending request for activating status of requested record.
	 * @param request object of request.
	 * @param id long value of request record.
	 */
	@PutMapping("/pending/active/{id}")
	public void activatePending(@RequestBody Request request, @PathVariable("id") Long id) {
		boolean authorized = false;
		Date actionedDate = new Date();
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		RequestType currentRequestType = this.getRequestType(request.getRequestType().getId());
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Request requestInfoById = this.requestRepository.findById(id);
		if (authorized) {
			if (requestInfoById == null) {
				throw new ResourceNotFoundException((long) 400, "Request information not found.");
			} else {
				if (requestInfoById.getRequestType().getRequestType().equals("rss")) {// company
				} else if (requestInfoById.getRequestType().getRequestMedia().equals(Constants.COMPANY)) { // web source
					// web for now nothing as data comes from feedly
				} else if(requestInfoById.getRequestType().getRequestType().equals("selection") || requestInfoById.getRequestType().getRequestType().equals("rejection")){// keyword
					KeywordMedia keywordMedia = KeywordMedia
							.valueOf(currentRequestType.getRequestMedia().toUpperCase());
					KeywordType keywordType = KeywordType.valueOf(currentRequestType.getRequestType().toUpperCase());
					Keyword existingKeyword = this.keywordRepository.doesKeywordExists(requestInfoById.getRequestedValue(),
							keywordMedia);
					Keyword newKeyword = new Keyword(existingKeyword.getKeyword(), existingKeyword.getType(),
							existingKeyword.getMedia(), true);
					newKeyword.setRequestedBy(requestInfoById.getRequestedBy());
					newKeyword.setRequestId(id);
					newKeyword.setType(keywordType);
					newKeyword.setComment(request.getComment());
					newKeyword.setApproveDate(actionedDate);
					newKeyword.setRequestedDate(requestInfoById.getRequestedDate());
					keywordCntrlr.approveKeywordRequest(existingKeyword, newKeyword, username,requestInfoById.getRequestedBy());
					this.email.setMessage(String.format("%s approved activation of '%s' %s %s keyword.",username,newKeyword.getKeyword(),newKeyword.getMedia(),newKeyword.getType()));
				}else if (requestInfoById.getRequestType().getRequestMedia().equals("twitter")) {// tw source
					SourceType sourceType = this.sourceRepository.findBySourceType(currentRequestType.getRequestType(),
							currentRequestType.getRequestMedia());
					String[] nameOfsource = requestInfoById.getRequestedValue().split("\\|");
					String receivedTwitterSourceName = nameOfsource[0];
					String twitterSourceName = receivedTwitterSourceName.trim();
					String receivedTwitterDisplayName = nameOfsource[1];
					TwitterSource existingSource = this.twitterSourceRepository.doesSourceExists(sourceType,
							twitterSourceName);
					existingSource.setSourceTypeName(currentRequestType.getRequestType());
					TwitterSource newTwitterSource = new TwitterSource(twitterSourceName,receivedTwitterDisplayName,existingSource.getSourceTypeName(), true);
					newTwitterSource.setSourceTypeName(currentRequestType.getRequestType());
					newTwitterSource.setRequestedBy(requestInfoById.getRequestedBy());
					newTwitterSource.setRequestId(id);
					newTwitterSource.setSourceType(sourceType);
					newTwitterSource.setComment(request.getComment());
					newTwitterSource.setApproveDate(actionedDate);
					newTwitterSource.setRequestedDate(requestInfoById.getRequestedDate());
					twitterCntrlr.approveTwitterSourceRequest(existingSource, newTwitterSource, username,requestInfoById.getRequestedBy());
					this.email.setMessage(String.format("%s approved activation of '%s' twitter %s.",username,newTwitterSource.getSourceName(),newTwitterSource.getSourceTypeName()));
				} else if (requestInfoById.getRequestType().getRequestMedia().equals("facebook")) {// fb source
					SourceType sourceType = this.sourceRepository.findBySourceType(currentRequestType.getRequestType(),
							currentRequestType.getRequestMedia());
					FacebookSource existingSource = this.facebookSourceRepository.doesSourceExists(sourceType,
							requestInfoById.getRequestedValue());
					existingSource.setSourceTypeName(currentRequestType.getRequestType());
					FacebookSource newFbSource = new FacebookSource(existingSource.getSourceName(),existingSource.getSourceTypeName(), true);
					newFbSource.setSourceTypeName(currentRequestType.getRequestType());
					newFbSource.setRequestedBy(requestInfoById.getRequestedBy());
					newFbSource.setRequestId(id);
					newFbSource.setSourceType(sourceType);
					newFbSource.setComment(request.getComment());
					newFbSource.setApproveDate(actionedDate);
					newFbSource.setRequestedDate(requestInfoById.getRequestedDate());
					facebookCntrlr.approveFacebookSourceRequest(existingSource, newFbSource, username,requestInfoById.getRequestedBy());
					this.email.setMessage(String.format("%s approved activation of '%s' facebook %s",username,newFbSource.getSourceName(),newFbSource.getSourceTypeName()));
				}
				this.addRequest(requestInfoById, RequestStatus.APPROVED, username, request.getComment(),actionedDate);
				//this.email.setMessage(String.format("deactivated"));
				this.emailService.send(this.email);
			}
		}
	}

	/**
	 * To accept pending request for in-activating status of requested record.
	 * @param request object of request.
	 * @param id long value of request record.
	 */
	@PutMapping("/pending/inactive/{id}")
	public void deactivatePending(@RequestBody Request request, @PathVariable("id") Long id) {
		boolean authorized = false;
		Date actionedDate = new Date();
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		RequestType currentRequestType = this.getRequestType(request.getRequestType().getId());
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Request requestInfoById = this.requestRepository.findById(id);
		if (authorized) {
			if (requestInfoById == null) {
				throw new ResourceNotFoundException((long) 400, "Request information not found.");
			} else {
				if (requestInfoById.getRequestType().getRequestType().equals("rss")) {// company
				} else if (requestInfoById.getRequestType().getRequestMedia().equals(Constants.COMPANY)) { // web source
					// web for now nothing as data comes from feedly
				}  else if(requestInfoById.getRequestType().getRequestType().equals("selection") || requestInfoById.getRequestType().getRequestType().equals("rejection")){// keyword
					KeywordMedia keywordMedia = KeywordMedia
							.valueOf(currentRequestType.getRequestMedia().toUpperCase());
					KeywordType keywordType = KeywordType.valueOf(currentRequestType.getRequestType().toUpperCase());
					Keyword existingKeyword = this.keywordRepository.doesKeywordExists(requestInfoById.getRequestedValue(),
							keywordMedia);
					Keyword newKeyword = new Keyword(existingKeyword.getKeyword(), existingKeyword.getType(),
							existingKeyword.getMedia(), false);
					newKeyword.setRequestedBy(requestInfoById.getRequestedBy());
					newKeyword.setRequestId(id);
					newKeyword.setType(keywordType);
					newKeyword.setComment(request.getComment());
					newKeyword.setRequestedDate(requestInfoById.getRequestedDate());
					newKeyword.setApproveDate(actionedDate);
					keywordCntrlr.approveKeywordRequest(existingKeyword, newKeyword, username,requestInfoById.getRequestedBy());
					this.email.setMessage(String.format("%s approved deactivation of '%s' %s %s keyword.",username,newKeyword.getKeyword(),newKeyword.getMedia(),newKeyword.getType()));
				}else if (requestInfoById.getRequestType().getRequestMedia().equals("twitter")) {// tw source
					SourceType sourceType = this.sourceRepository.findBySourceType(currentRequestType.getRequestType(),
							currentRequestType.getRequestMedia());
					String[] nameOfsource = requestInfoById.getRequestedValue().split("\\|");
					String receivedTwitterSourceName = nameOfsource[0];
					String twitterSourceName = receivedTwitterSourceName.trim();
					String receivedTwitterDisplayName = nameOfsource[1];
					TwitterSource existingSource = this.twitterSourceRepository.doesSourceExists(sourceType,
							twitterSourceName);
					existingSource.setSourceTypeName(currentRequestType.getRequestType());
					TwitterSource newTwitterSource = new TwitterSource(twitterSourceName,receivedTwitterDisplayName,existingSource.getSourceTypeName(), false);
					newTwitterSource.setSourceTypeName(currentRequestType.getRequestType());
					newTwitterSource.setRequestedBy(requestInfoById.getRequestedBy());
					newTwitterSource.setRequestId(id);
					newTwitterSource.setSourceType(sourceType);
					newTwitterSource.setComment(request.getComment());
					newTwitterSource.setRequestedDate(requestInfoById.getRequestedDate());
					newTwitterSource.setApproveDate(actionedDate);
					twitterCntrlr.approveTwitterSourceRequest(existingSource, newTwitterSource, username,requestInfoById.getRequestedBy());
					this.email.setMessage(String.format("%s approved deactivation of '%s' twitter %s.",username,newTwitterSource.getSourceName(),newTwitterSource.getSourceTypeName()));
				} else if (requestInfoById.getRequestType().getRequestMedia().equals("facebook")) {// fb source
					SourceType sourceType = this.sourceRepository.findBySourceType(currentRequestType.getRequestType(),
							currentRequestType.getRequestMedia());
					FacebookSource existingSource = this.facebookSourceRepository.doesSourceExists(sourceType,
							requestInfoById.getRequestedValue());
					existingSource.setSourceTypeName(currentRequestType.getRequestType());
					FacebookSource newFbSource = new FacebookSource(existingSource.getSourceName(),existingSource.getSourceTypeName(), false);
					newFbSource.setSourceTypeName(currentRequestType.getRequestType());
					newFbSource.setRequestedBy(requestInfoById.getRequestedBy());
					newFbSource.setRequestId(id);
					newFbSource.setSourceType(sourceType);
					newFbSource.setComment(request.getComment());
					newFbSource.setApproveDate(actionedDate);
					newFbSource.setRequestedDate(requestInfoById.getRequestedDate());
					facebookCntrlr.approveFacebookSourceRequest(existingSource, newFbSource, username,requestInfoById.getRequestedBy());
					this.email.setMessage(String.format("%s approved deactivation of '%s' facebook %s",username,newFbSource.getSourceName(),newFbSource.getSourceTypeName()));
				}
				this.addRequest(requestInfoById, RequestStatus.APPROVED, username, request.getComment(),actionedDate);
				//this.email.setMessage(String.format("%s approved deactivation of '%s' %s %s",username));
				this.emailService.send(this.email);
			}
		}
	}

	/**
	 * To accept pending request of company record.
	 * @param request object of request.
	 * @param id long value of request record.
	 */
	@PutMapping("/pending/company/{id}")
	public void updatePendingGram(@RequestBody Request request, @PathVariable("id") Long id) {
		boolean authorized = false;
		Date actionedDate = new Date();
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		// RequestType currentRequestType
		// =this.getRequestType(request.getRequestType().getId());
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Request existingCompanyRequest = this.requestRepository.findById(id);
		String companyGrams = request.getRequestedValue().split(":")[1];
		if (existingCompanyRequest == null) {
			throw new ResourceNotFoundException((long) 400, "Company details not found.");
		} else {
			// id = existingCompanyRequest.
			String receivedCompanyId = existingCompanyRequest.getRequestedValue().split(":")[0];
			long companyId = Long.parseLong(receivedCompanyId.trim());
			Company existingCompanyInfo = companyRepository.findById(companyId);
			if (authorized) {
				existingCompanyInfo.setEndDate(new Date());
				this.companyRepository.save(existingCompanyInfo);
				Company updatedCompany = new Company(existingCompanyInfo, true);
				updatedCompany.setRequestId(id);
				updatedCompany.setRequestedBy(existingCompanyRequest.getRequestedBy());
				updatedCompany.setRequestedDate(existingCompanyRequest.getRequestedDate());
				updatedCompany.setComment(request.getComment());
				updatedCompany.setGrams(companyGrams);
				updatedCompany.setApproveDate(actionedDate);
				companyCntrlr.approveCompanyRequest(existingCompanyInfo, updatedCompany, username,existingCompanyRequest.getRequestedBy());
				this.addRequest(existingCompanyRequest, RequestStatus.APPROVED, username, request.getComment(),actionedDate);
				this.email.setMessage(String.format("%s approved '%s' company gram for %s",username,updatedCompany.getGrams(),updatedCompany.getName() ));
				this.emailService.send(this.email);
			}
		}
	}
}
