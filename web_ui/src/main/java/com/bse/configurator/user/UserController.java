package com.bse.configurator.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bse.configurator.exception.ResourceNotFoundException;

/**
 * 
 * @author Bharat.Pattani
 * Class of User Controller
 * To perform all operations required by user.
 *
 */
@RestController
@RequestMapping("/users")
public class UserController {

	@Value("${v2t.report.address}")
	String v2tReportAddress;
	
    private ApplicationUserRepository applicationUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;
    /**
     * To set user and password
     * @param applicationUserRepository
     * @param bCryptPasswordEncoder
     */
    public UserController(ApplicationUserRepository applicationUserRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * To register new user
     * @param user
     */
    @PostMapping("/sign-up")
    public void signUp(@RequestBody ApplicationUser user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        applicationUserRepository.save(user);
    }
    
    /**
     * To change password of existing user
     * @param user object of application user.
     * @throws IOException
     */
    @PutMapping("/change-password")
    public void changePassword(@RequestBody ApplicationUser user) throws IOException {
    	String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	Authentication auth = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(username, user.getPassword(),
						(Collection<? extends GrantedAuthority>) new ArrayList<SimpleGrantedAuthority>()));
    	if (auth.isAuthenticated()) {
    		if (!user.getNewPassword().equals(user.getConfirmNewPassword())) {
    			throw new ResourceNotFoundException((long) 400, "New password and confirm new password should be same.");
    		} else {
    			ApplicationUser existingUser = applicationUserRepository.findByUsername(username);
    			existingUser.setPassword(bCryptPasswordEncoder.encode(user.getNewPassword()));
    	        applicationUserRepository.save(existingUser);
    		}
    	}
    }
    /**
     * To Get users details
     */
    @GetMapping("/")
    public Map<String, Object> getUserDetails() {
    	String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	ApplicationUser user = this.applicationUserRepository.findByUsername(username);
    	Map<String, Object> output = new HashMap<String,Object>();
    	output.put("first_name", user.getFirstName());
    	output.put("last_name", user.getLastName());
    	output.put("roles", user.getUserRole());
    	output.put("v2t_report_address",this.v2tReportAddress);
    	return output;
    	
    }
    
}