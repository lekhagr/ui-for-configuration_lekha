package com.bse.configurator.user;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Bharat.Pattani
 * Class to create user audit.
 * fields:
 * id[long] Auto-increment Id
 * loggedinAt[Date]
 * username[String]
 * clientAddress[String]
 */

@Entity
@Table(name = "application_user_audit")
public class ApplicationUserAudit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private Date loggedinAt = new Date();
	private String username, clientAddress;
	
	

	public ApplicationUserAudit() {
	}

	public ApplicationUserAudit(String username,String clientAddress) {
		this.username = username;
		this.clientAddress = clientAddress;
	}

	public long getId() {
		return id;
	}

	public Date getLoggedinAt() {
		return loggedinAt;
	}

	public void setLoggedinAt(Date loggedinAt) {
		this.loggedinAt = loggedinAt;
	}

	public String getClientAddress() {
		return clientAddress;
	}

	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
