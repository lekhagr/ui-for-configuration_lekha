package com.bse.configurator.user;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

//import static java.util.Collections.emptyList;

import java.util.ArrayList;

/**
 * Service to get user details.
 * @author Bharat.Pattani
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private ApplicationUserRepository applicationUserRepository;

    /**
     * To set user details.
     * @param applicationUserRepository
     */
    public UserDetailsServiceImpl(ApplicationUserRepository applicationUserRepository) {
        this.applicationUserRepository = applicationUserRepository;
    }

    /**
     * To load user by it's name.
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ApplicationUser applicationUser = applicationUserRepository.findByUsername(username);
        if (applicationUser == null) {
            throw new UsernameNotFoundException(username);
        }
        /**
         * To set user details with authorities.  
         */
        Collection<GrantedAuthority> grantedAuthority = new ArrayList<>();
        GrantedAuthority authority = new SimpleGrantedAuthority(applicationUser.getUserRole());
        grantedAuthority.add(authority);

        return new User(applicationUser.getUsername(), applicationUser.getPassword(), grantedAuthority);
    }
}