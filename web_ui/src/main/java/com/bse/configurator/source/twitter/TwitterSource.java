package com.bse.configurator.source.twitter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


import com.bse.configurator.source.SourceType;
import com.bse.configurator.source.twitter.validator.TwitterSourceType;
import com.bse.configurator.source.twitter.validator.TwitterSourceName;
/**
 * @author bharat.pattani
 * It is a FacebookSource class.
 * Fields:
 * id[long] Auto-increment Id
 * requested[boolean]
 * sourceType[object]
 * sourceTypeName[String]
 * sourceName[String]
 * displayName[String]
 * underReview[Boolean]
 * status[Boolean]
 * startDate[Date]
 * endDate[Date]
 * requestedDate[Date]
 * approveDate[Date]
 * approvedBy[String]
 * requestedBy[String]
 * comment[String]
 * requestId[long]
 */
@Entity
@Table(name = "twitter_source")
@TwitterSourceName(message = "Invalid Source Name (sourceName).")
public class TwitterSource {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="twitter_source_id")
	private long id;
	@ManyToOne(cascade=CascadeType.MERGE)
    @JoinColumn(name = "twitter_source_type_id",nullable=false)
	private SourceType sourceType;
	
	@org.springframework.data.annotation.Transient
	private Boolean requested;
	
	@Transient
	@TwitterSourceType(message = "Invalid source type, souceTypeName must be either 'hashtag' or 'handle'.")
	private String sourceTypeName;
	public String getSourceTypeName() {
		return sourceTypeName;
	}
	public void setSourceTypeName(String sourceTypeName) {
		this.sourceTypeName = sourceTypeName;
	}

	@Column(name="twitter_source_name")
	private String sourceName;
	
	@Column(name="display_name")
	private String displayName;
	
	@Column(name="twitter_source_status")
	private Boolean status=true;
	
	private Date startDate=new Date();
	private Date endDate = new Date();
	private Date requestedDate = null;
	private Date approveDate = new Date();
	@Transient
	private boolean underReview;

	
	@Column(name="approved_by")
	private String approvedBy;
	
	@Column(name="requested_by")
	private String requestedBy;
	
	@Column(name="request_id")
	private long requestId;
	
	@Column(name="comment")
	private String comment;
	
	public String getSourceName() {
		return sourceName.toLowerCase();
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName.toLowerCase();
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getRequestedDate() {
		return requestedDate;
	}
	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}
	public Date getApproveDate() {
		return approveDate;
	}
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}
	public String getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}
	public String getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
	public long getRequestId() {
		return requestId;
	}
	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public TwitterSource() {
		try {
			this.endDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("9999-12-31 23:59:59");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	public TwitterSource(String sourceName,String sourceTypeName,Boolean status) {
		this();
		this.sourceName = sourceName;
		this.displayName = displayName;
		this.sourceTypeName = sourceTypeName;
		this.status = status;
	}
	
	public TwitterSource(String sourceName,String displayName,String sourceTypeName,Boolean status) {
		this();
		this.sourceName = sourceName;
		this.displayName = displayName;
		this.sourceTypeName = sourceTypeName;
		this.status = status;
	}
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public SourceType getSourceTypes() {
        return this.sourceType;
    }
	
	public void setSourceType(SourceType sourceType) {
		this.sourceType=sourceType;
	}
	public boolean isUnderReview() {
		return underReview;
	}
	public void setUnderReview(boolean underReview) {
		this.underReview = underReview;
	}
	public Boolean getRequested() {
		return requested;
	}
	public void setRequested(Boolean requested) {
		this.requested = requested;
	}
	
	public String getdisplayName() {
		return displayName;
	}
	public void setdisplayName(String displayName) {
		this.displayName = displayName;
	}

}
