package com.bse.configurator.source.twitter;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bse.configurator.source.SourceType;
import com.bse.configurator.source.twitter.TwitterSource;

/**
 * 
 * @author Bharat.Pattani
 *
 */
@Repository
public interface TwitterSourceRepository extends JpaRepository<TwitterSource, Long>{
	
	/**
	 * @return all records of twitter source.
	 */
	@Query(nativeQuery=true,value="SELECT CASE WHEN t2.existing_value IS NULL THEN false ELSE true END as requested,t1.* from (SELECT s.source_type as source_type,tw.* FROM public.twitter_source tw INNER JOIN source_type s ON tw.twitter_source_type_id = s.id) t1 LEFT OUTER JOIN (SELECT rt.request_media,rt.request_type,r.* from public.request r INNER JOIN public.request_type rt ON r.request_type_id = rt.id where rt.request_media='twitter' and rt.request_type in ('handle','hashtag') and r.request_status='PENDING') t2 ON t1.twitter_source_name=t2.existing_value AND t1.source_type=t2.request_type where t1.end_date='9999-12-31 23:59:59'")
	List<TwitterSource> findAllRecords();
	
	/**
	 * @param status boolean value as true/false
	 * @return all records based on status as true/false
	 */
	@Query(nativeQuery=true,value="SELECT CASE WHEN t2.existing_value IS NULL THEN false ELSE true END as requested,t1.* from (SELECT s.source_type as source_type,tw.* FROM public.twitter_source tw INNER JOIN source_type s ON tw.twitter_source_type_id = s.id) t1 LEFT OUTER JOIN (SELECT rt.request_media,rt.request_type,r.* from public.request r INNER JOIN public.request_type rt ON r.request_type_id = rt.id where rt.request_media='twitter' and rt.request_type in ('handle','hashtag') and r.request_status='PENDING') t2 ON t1.twitter_source_name=t2.existing_value AND t1.source_type=t2.request_type where t1.end_date='9999-12-31 23:59:59'  and  t1.twitter_source_status = ?1")
	List<TwitterSource> findRecordsByStatus(Boolean status);

	/**
	 * To find if records exists.
	 * @param sourceType type of twitter source.
	 * @param sourceName name of twitter source.
	 * @return twitter source record if exists.
	 */
	@Query("select t from TwitterSource t where t.endDate='9999-12-31 23:59:59' and t.sourceType=?1  and t.sourceName=?2")
	TwitterSource doesSourceExists(SourceType sourceType,String sourceName);
	
	/**
	 * To find record by status.
	 * @param id long value to identify record.
	 * @param sourceType type of twitter source.
	 * @param sourceName name of twitter source.
	 * @return twitter source record if status true.
	 */
	@Query("select t from TwitterSource t where t.endDate='9999-12-31 23:59:59' and t.id = ?1 and t.sourceType=?2 and t.status = true and t.sourceName=?3")
	TwitterSource findByActiveSource(Long id,SourceType sourceType,String sourceName);
	
	/**
	 * To find record by status.
	 * @param id long value to identify record.
	 * @param sourceType type of twitter source.
	 * @param sourceName name of twitter source.
	 * @return twitter source record if status false.
	 */
	@Query("select t from TwitterSource t where t.endDate='9999-12-31 23:59:59' and t.id = ?1 and t.sourceType=?2 and t.status = false and t.sourceName=?3")
	TwitterSource findByInactiveSource(Long id,SourceType sourceType,String sourceName);

}
