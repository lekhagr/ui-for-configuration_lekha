package com.bse.configurator.source.twitter.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
/**
 * 
 * @author Bharat.Pattani
 * To know if twitter source name is valid or not.
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { TwitterSourceNameValidator.class })
public @interface TwitterSourceName {
	String message() default "Invalid Source Name";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
