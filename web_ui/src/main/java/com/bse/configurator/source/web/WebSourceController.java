package com.bse.configurator.source.web;

import java.util.List;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bse.configurator.source.SourceType;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/source/web")
public class WebSourceController {
	private WebSourceRepository webSourceRepository;
	private SourceType sourceType;
	public WebSourceController(WebSourceRepository webSourceRepository) {
		this.webSourceRepository = webSourceRepository;
		this.sourceType= new SourceType("rss","web");
	
	}

	@PostMapping
	public void addSource(@RequestBody WebSource source) {
		String userName = (String) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		source.setCreatedBy(userName);
		source.setSourceType(this.sourceType);
		this.webSourceRepository.save(source);
	}

	@GetMapping
	public List<WebSource> getSources() {
		return this.webSourceRepository.findAll();
	}
}
