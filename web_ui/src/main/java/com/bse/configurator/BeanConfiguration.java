package com.bse.configurator;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class BeanConfiguration {

	
	@Autowired
	private DataSource ds;
	
	@Bean
	public JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate(ds);
	}
	
}
